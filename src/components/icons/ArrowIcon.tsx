export function ArrowIcon({
  type,
}: {
  type: "up" | "right" | "down" | "left";
}) {
  let deg = 0;
  switch (type) {
    case "up":
      deg = 180;
      break;
    case "right":
      deg = -90;
      break;
    case "down":
      deg = 0;
      break;
    case "left":
      deg = 90;
      break;

    default:
      break;
  }
  return (
    <svg
      width="8"
      height="8"
      viewBox="0 0 12 8"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      transform={`rotate(${deg})`}
    >
      <path
        d="M1 1.5L6 6.5L11 1.5"
        stroke="#3A3A3C"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
