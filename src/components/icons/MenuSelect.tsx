const MenuSelect = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="13"
      height="20"
      viewBox="0 0 13 20"
      fill="none"
    >
      <g clipPath="url(#clip0_3823_4764)">
        <path
          d="M12.1335 9.63281L7.16865 14.6328C6.8913 14.8789 6.57099 15 6.25068 15C5.93037 15 5.61083 14.8779 5.36669 14.6338L0.40185 9.63379C0.00944342 9.27734 -0.0976035 8.73828 0.0959122 8.27344C0.289428 7.80859 0.7456 7.5 1.25068 7.5H11.2155C11.721 7.5 12.1772 7.80395 12.371 8.27148C12.5647 8.73902 12.4929 9.27734 12.1335 9.63281Z"
          fill="#3A3A3C"
        />
      </g>
      <defs>
        <clipPath id="clip0_3823_4764">
          <rect width="12.5" height="20" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export { MenuSelect };
