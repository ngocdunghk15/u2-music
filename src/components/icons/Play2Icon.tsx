export function Play2Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="20"
      viewBox="0 0 16 20"
      fill="none"
    >
      <g clipPath="url(#clip0_3760_5527)">
        <path
          d="M14.6016 8.39848C15.1602 8.74223 15.5 9.3477 15.5 10C15.5 10.6524 15.1602 11.2579 14.6016 11.5664L3.35273 18.4414C2.77383 18.8282 2.04922 18.8438 1.45781 18.5118C0.866289 18.1797 0.5 17.5547 0.5 16.875V3.12504C0.5 2.44691 0.866289 1.82152 1.45781 1.48949C2.04922 1.15785 2.77383 1.17152 3.35273 1.52504L14.6016 8.39848Z"
          fill="#8E8E93"
        />
      </g>
      <defs>
        <clipPath id="clip0_3760_5527">
          <rect
            width="15"
            height="20"
            fill="white"
            transform="translate(0.5)"
          />
        </clipPath>
      </defs>
    </svg>
  );
}
