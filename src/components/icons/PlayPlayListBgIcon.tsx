export function PlayPlayListBgIcon() {
  return (
    <svg
      width="310"
      height="331"
      viewBox="0 0 220 221"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="110" cy="110.396" r="110" fill="#FFEFAD" />
    </svg>
  );
}
