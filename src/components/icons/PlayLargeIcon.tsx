export function PlayLargeIcon() {
  return (
    <svg
      width="240"
      height="240"
      viewBox="0 0 240 240"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_3827_6317)">
        <rect width="240" height="240" rx="12" fill="#EDECD6" />
        <path
          d="M9.5 72.6533V167.347"
          stroke="url(#paint0_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M26.5 93.8779V146.123"
          stroke="url(#paint1_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M43.5 80V160"
          stroke="url(#paint2_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M60.5 53.0615V186.939"
          stroke="url(#paint3_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M77.5 66.1221V173.877"
          stroke="url(#paint4_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M94.5 40V200"
          stroke="url(#paint5_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M111.5 95.5107V144.49"
          stroke="url(#paint6_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M128.5 48.1631V191.837"
          stroke="url(#paint7_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M145.5 57.96V182.042"
          stroke="url(#paint8_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M162.5 72.6533V167.347"
          stroke="url(#paint9_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M179.5 93.8779V146.123"
          stroke="url(#paint10_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M196.5 79.1836V160.816"
          stroke="url(#paint11_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M213.5 95.5107V144.49"
          stroke="url(#paint12_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M230.5 48.1631V191.837"
          stroke="url(#paint13_linear_3827_6317)"
          strokeWidth="6"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <circle cx="120" cy="120" r="60" fill="#FFCD00" />
        <path
          d="M108.02 95.1234C107.716 94.9434 107.37 94.8469 107.017 94.8438C106.664 94.8407 106.317 94.9311 106.01 95.1058C105.703 95.2806 105.448 95.5333 105.271 95.8385C105.094 96.1437 105 96.4904 105 96.8434V143.157C105 143.51 105.094 143.856 105.271 144.162C105.448 144.467 105.703 144.72 106.01 144.894C106.317 145.069 106.664 145.159 107.017 145.156C107.37 145.153 107.716 145.057 108.02 144.877L147.097 121.72C147.395 121.543 147.642 121.291 147.814 120.99C147.986 120.688 148.076 120.347 148.076 120C148.076 119.653 147.986 119.312 147.814 119.01C147.642 118.709 147.395 118.457 147.097 118.28L108.02 95.1234Z"
          fill="white"
          stroke="white"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M66.048 214.192H71.56V216H64.12V204.48H71.56V206.288H66.048V209.128H70.6V210.936H66.048V214.192ZM78.0074 207.112C81.1914 207.112 81.4074 210.152 81.4074 211.176V216H79.4554V211.84C79.4554 210.776 79.2794 208.928 77.4394 208.928C76.4554 208.928 75.4154 209.464 75.4154 211.56V216H73.4634V207.36H75.1834V208.32C75.8154 207.576 76.7594 207.112 78.0074 207.112ZM84.879 206.32C84.215 206.32 83.719 205.816 83.719 205.168C83.719 204.52 84.215 204.016 84.879 204.016C85.543 204.016 86.039 204.52 86.039 205.168C86.039 205.816 85.543 206.32 84.879 206.32ZM83.527 219.84H82.215V218.032H82.623C83.567 218.032 83.919 217.592 83.919 216.736V207.36H85.839V217.6C85.839 218.992 84.975 219.84 83.527 219.84ZM91.9204 216.24C89.3204 216.24 87.6244 214.352 87.6244 211.68C87.6244 208.968 89.3684 207.12 91.9204 207.12C94.5284 207.12 96.2324 209.008 96.2324 211.68C96.2324 214.376 94.5044 216.24 91.9204 216.24ZM91.9204 214.432C93.4484 214.432 94.1924 213.304 94.1924 211.68C94.1924 210.008 93.4324 208.928 91.9204 208.928C90.3684 208.928 89.6644 210.048 89.6644 211.68C89.6644 213.36 90.4324 214.432 91.9204 214.432ZM98.7545 207.36L101.163 213.544L103.515 207.36H105.443L100.627 219.84H98.8185L100.243 215.944L96.7465 207.36H98.7545ZM114.903 208.872H112.367V212.36C112.367 213.048 112.335 213.584 112.543 213.912C112.927 214.552 113.823 214.544 114.903 214.384V216C113.191 216.328 111.447 216.176 110.775 214.936C110.375 214.192 110.447 213.464 110.447 212.472V208.872H108.879V207.36H110.447V204.96H112.367V207.36H114.903V208.872ZM120.139 216.24C117.539 216.24 115.843 214.352 115.843 211.68C115.843 208.968 117.587 207.12 120.139 207.12C122.747 207.12 124.451 209.008 124.451 211.68C124.451 214.376 122.723 216.24 120.139 216.24ZM120.139 214.432C121.667 214.432 122.411 213.304 122.411 211.68C122.411 210.008 121.651 208.928 120.139 208.928C118.587 208.928 117.883 210.048 117.883 211.68C117.883 213.36 118.651 214.432 120.139 214.432ZM132.142 207.984V207.36H133.83V216.416C133.83 216.864 133.798 217.248 133.71 217.632C133.318 219.304 131.798 220.08 129.846 220.08C128.406 220.08 127.086 219.376 126.414 218.2L128.19 217.32C128.51 217.944 129.174 218.248 129.854 218.248C131.054 218.248 131.926 217.616 131.902 216.384V215.56C131.318 215.992 130.574 216.24 129.678 216.24C127.286 216.24 125.734 214.272 125.734 211.68C125.734 209.056 127.302 207.12 129.75 207.12C130.734 207.12 131.534 207.432 132.142 207.984ZM129.974 214.512C131.526 214.512 132.142 213.352 132.142 211.68C132.142 209.992 131.51 208.848 130.038 208.848C128.486 208.848 127.774 210.112 127.774 211.68C127.774 213.264 128.47 214.512 129.974 214.512ZM143.913 212.216H137.657C137.809 213.64 138.577 214.432 139.889 214.432C140.833 214.432 141.537 214 141.913 213.2L143.817 213.776C143.153 215.344 141.665 216.24 140.001 216.24C137.377 216.24 135.593 214.4 135.593 211.776C135.593 208.944 137.345 207.12 139.889 207.12C142.569 207.12 144.137 209.104 143.913 212.216ZM139.969 208.808C138.673 208.808 137.929 209.48 137.705 210.76H141.953C141.793 209.408 141.161 208.808 139.969 208.808ZM150.762 208.872H148.226V212.36C148.226 213.048 148.194 213.584 148.402 213.912C148.786 214.552 149.682 214.544 150.762 214.384V216C149.05 216.328 147.306 216.176 146.634 214.936C146.234 214.192 146.306 213.464 146.306 212.472V208.872H144.738V207.36H146.306V204.96H148.226V207.36H150.762V208.872ZM157.148 207.112C160.332 207.112 160.548 210.152 160.548 211.176V216H158.596V211.84C158.596 210.776 158.42 208.928 156.58 208.928C155.596 208.928 154.556 209.464 154.556 211.56V216H152.604V204.48H154.324V208.32C154.956 207.576 155.9 207.112 157.148 207.112ZM170.476 212.216H164.22C164.372 213.64 165.14 214.432 166.452 214.432C167.396 214.432 168.1 214 168.476 213.2L170.38 213.776C169.716 215.344 168.228 216.24 166.564 216.24C163.94 216.24 162.156 214.4 162.156 211.776C162.156 208.944 163.908 207.12 166.452 207.12C169.132 207.12 170.7 209.104 170.476 212.216ZM166.532 208.808C165.236 208.808 164.492 209.48 164.268 210.76H168.516C168.356 209.408 167.724 208.808 166.532 208.808ZM174.829 207.816C175.469 207.376 176.349 207.24 177.125 207.36V209.16C176.605 209.016 175.877 209.056 175.341 209.368C174.549 209.8 174.197 210.608 174.197 211.608V216H172.261V207.36H173.965V208.744C174.181 208.368 174.469 208.04 174.829 207.816Z"
          fill="#3A3A3C"
        />
      </g>
      <rect
        x="0.5"
        y="0.5"
        width="239"
        height="239"
        rx="11.5"
        stroke="#ECECEC"
      />
      <defs>
        <linearGradient
          id="paint0_linear_3827_6317"
          x1="9.5"
          y1="72.6533"
          x2="9.5"
          y2="167.347"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_3827_6317"
          x1="26.5"
          y1="93.8779"
          x2="26.5"
          y2="146.123"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_3827_6317"
          x1="43.5"
          y1="80"
          x2="43.5"
          y2="160"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_3827_6317"
          x1="60.5"
          y1="53.0615"
          x2="60.5"
          y2="186.939"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_3827_6317"
          x1="77.5"
          y1="66.1221"
          x2="77.5"
          y2="173.877"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_3827_6317"
          x1="94.5"
          y1="40"
          x2="94.5"
          y2="200"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_3827_6317"
          x1="111.5"
          y1="95.5107"
          x2="111.5"
          y2="144.49"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint7_linear_3827_6317"
          x1="128.5"
          y1="48.1631"
          x2="128.5"
          y2="191.837"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint8_linear_3827_6317"
          x1="145.5"
          y1="57.96"
          x2="145.5"
          y2="182.042"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint9_linear_3827_6317"
          x1="162.5"
          y1="72.6533"
          x2="162.5"
          y2="167.347"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint10_linear_3827_6317"
          x1="179.5"
          y1="93.8779"
          x2="179.5"
          y2="146.123"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint11_linear_3827_6317"
          x1="196.5"
          y1="79.1836"
          x2="196.5"
          y2="160.816"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint12_linear_3827_6317"
          x1="213.5"
          y1="95.5107"
          x2="213.5"
          y2="144.49"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint13_linear_3827_6317"
          x1="230.5"
          y1="48.1631"
          x2="230.5"
          y2="191.837"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <clipPath id="clip0_3827_6317">
          <rect width="240" height="240" rx="12" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
}
