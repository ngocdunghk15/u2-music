export * from "@/components/icons/BellIcon";
export * from "@/components/icons/ClockIcon";
export * from "@/components/icons/ListMusicIcon";
export * from "@/components/icons/LogInIcon";
export * from "@/components/icons/LogoutIcon";
export * from "@/components/icons/MusicIcon";
export * from "@/components/icons/NextIcon";
export * from "@/components/icons/PauseIcon";
export * from "@/components/icons/PlusIcon";
export * from "@/components/icons/PreviousIcon";
export * from "@/components/icons/QuestionIcon";
export * from "@/components/icons/RepeatIcon";
export * from "@/components/icons/SettingIcon";
export * from "@/components/icons/ShuffleIcon";
export * from "@/components/icons/UserIcon";
export * from "@/components/icons/VolumnHighIcon";
export * from "@/components/icons/NavArrowDownIcon";
export * from "@/components/icons/AngleUpIcon";
export * from "@/components/icons/Play3Icon";
export * from "@/components/icons/Music3Icon";
export * from "@/components/icons/ListMusic2Icon";
export * from "@/components/icons/SyncIcon";
export * from "@/components/icons/DropboxIcon";
export * from "@/components/icons/GoogleDriveIcon";
export * from "@/components/icons/Shuffle2Icon";
export * from "@/components/icons/ArrowIcon";
export * from "@/components/icons/MoreIcon";
export * from "@/components/icons/XMark1Icon";
export * from "@/components/icons/AngleRight1Icon";
export * from "@/components/icons/SearchIcon";
export * from "@/components/icons/User2Icon";
export * from "@/components/icons/CloseIcon";
export * from "@/components/icons/Play2Icon";
export * from "@/components/icons/PlayLargeIcon";
export * from "@/components/icons/AddFriendIcon";
export * from "@/components/icons/UserGroupIcon";
export * from "@/components/icons/LeaveIcon";
export * from "@/components/icons/CopyIcon";
export * from "@/components/icons/HeadphoneIcon";
export * from "@/components/icons/SquarePlus2Icon";
import BgGlobal from "./BgGlobal.png"


const images = {
    BgGlobal
}
export { images };
