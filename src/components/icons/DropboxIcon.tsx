export function DropboxIcon() {
  return (
    <svg
      width="28"
      height="29"
      viewBox="0 0 28 29"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8.74954 5.36938L3.5 8.71389L8.74954 12.0584L14 8.71389L8.74954 5.36938Z"
        fill="#0061FF"
      />
      <path
        d="M19.25 5.36938L14 8.7142L19.25 12.059L24.5 8.7142L19.25 5.36938Z"
        fill="#0061FF"
      />
      <path
        d="M8.74954 18.748L3.5 15.4035L8.74954 12.059L14 15.4035L8.74954 18.748Z"
        fill="#0061FF"
      />
      <path
        d="M19.25 12.059L14 15.4038L19.2499 18.7486L24.4999 15.4038L19.25 12.059Z"
        fill="#0061FF"
      />
      <path
        d="M14.0004 23.2146L8.74998 19.8701L14.0004 16.5256L19.2499 19.8701L14.0004 23.2146Z"
        fill="#0061FF"
      />
    </svg>
  );
}
