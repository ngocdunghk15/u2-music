export function PlayPlayListBgIconEmpty() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="300"
      height="300"
      viewBox="0 0 250 235"
      fill="none"
    >
      <circle cx="110" cy="110.396" r="110" fill="#FFEFAD" />
    </svg>
  );
}
