const SelectConfig = {
  controlHeightLG: 42,
  controlHeight: 38,
  controlHeightSM: 32,
}

export {SelectConfig};