const ModalConfig = {
  paddingContentHorizontalLG: 24,
  paddingMD: 24,
  borderRadiusLG: 12,
}

export {ModalConfig};