const InputConfig = {
  colorTextBase: "#3A3A3C",
  colorTextPlaceholder: "#8E8E93",
  controlHeightLG: 48,
  borderRadiusLG: 24,
  controlPaddingHorizontal: 20,

  controlHeight: 40,
  borderRadius: 16,

  controlHeightSM: 32,
  borderRadiusSM: 48,
  controlPaddingHorizontalSM: 12,
  algorithm: true, // Enable algorithm
}

export {InputConfig};