const ButtonConfig = {
  borderRadiusLG: 34,
  colorBorder: "#ECECEC",
  colorBgContainerDisabled: "#E3E3E3",
  algorithm: true, // Enable algorithm
}

export {ButtonConfig};