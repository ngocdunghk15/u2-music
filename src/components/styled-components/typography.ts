const TypographyConfig = {
  fontSizeHeading1: 32,
  fontSizeHeading2: 28,
  fontSizeHeading3: 24,
  fontSizeHeading4: 20,
  fontSizeHeading5: 18,
}

export {TypographyConfig};