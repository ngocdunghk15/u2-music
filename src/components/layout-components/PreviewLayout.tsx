import React, {useState} from 'react';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons';
import {Layout, Menu, Button, theme, Typography} from 'antd';
import {Outlet, useNavigate} from "react-router-dom";

const {Header, Sider, Content} = Layout;

const PreviewLayout: React.FC = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: {colorBgContainer},
  } = theme.useToken();
  const navigate = useNavigate();

  return (
    <Layout style={{height: "100%"}}>
      <Sider style={{background: colorBgContainer}} trigger={null} collapsible collapsed={collapsed}>
        <Typography.Title level={3} className={'pl-6'}>Preview</Typography.Title>
        <Menu
          mode="inline"
          defaultSelectedKeys={['1']}
          onClick={(e) => {
            navigate(e.key)
          }}
          items={[
            {
              key: '/preview/button',
              label: 'Preview button',
            },
            {
              key: '/preview/typography',
              label: 'Preview typography',
            },
            {
              key: '/preview/input',
              label: 'Preview input',
            },
            {
              key: '2',
              icon: <VideoCameraOutlined/>,
              label: 'nav 2',
            },
            {
              key: '3',
              icon: <UploadOutlined/>,
              label: 'nav 3',
            },
          ]}
        />
      </Sider>
      <Layout>
        <Header style={{padding: 0, background: colorBgContainer}}>
          <Button
            type="text"
            icon={collapsed ? <MenuUnfoldOutlined/> : <MenuFoldOutlined/>}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: '16px',
              width: 64,
              height: 64,
            }}
          />
        </Header>
        <Content
          style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          <Outlet/>
        </Content>
      </Layout>
    </Layout>
  );
};

export default PreviewLayout;