import {Button, Col, Row} from "antd";
import {WhiteDropboxIcon} from "@/components/icons/WhiteDropboxIcon.tsx";

export default function PreviewButton() {
  return <Row gutter={[12, 4]}>
    <Col>
      <Button size={'large'}>Default</Button>
    </Col>
    <Col>
      <Button type={'primary'} size={'large'}>Primary</Button>
    </Col>
    <Col>
      <Button disabled size={'large'}>Disabled</Button>
    </Col>
    <Col>
      <Button className={'d-flex align-center'} style={{background: '#1877F2'}} type="primary" icon={<WhiteDropboxIcon/>} size={'large'}>
        Dropbox
      </Button>
    </Col>
  </Row>
}
