import {Input, Select} from "antd";

export default function PreviewInput() {
  return (
    <div >
      <Input size={"large"}/>
      <Input className={"mt-4"}  size={"middle"}/>
      <Input className={"mt-4"}  size={"small"}/>


      <Select
        size={"small"}
        className={"mt-4"}
        defaultValue="lucy"
        style={{ width: 120 }}
        options={[
          { value: 'jack', label: 'Jack' },
          { value: 'lucy', label: 'Lucy' },
        ]}
      />

      <Select
        size={"middle"}
        className={"mt-4"}
        defaultValue="lucy"
        style={{ width: 120 }}
        options={[
          { value: 'jack', label: 'Jack' },
          { value: 'lucy', label: 'Lucy' },
        ]}
      />

      <Select
        size={"large"}
        className={"mt-4"}
        defaultValue="lucy"
        style={{ width: 120 }}
        options={[
          { value: 'jack', label: 'Jack' },
          { value: 'lucy', label: 'Lucy' },
        ]}
      />
    </div>
  )
}
