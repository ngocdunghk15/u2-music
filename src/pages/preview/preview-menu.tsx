import {Menu} from "antd";
import {ClockIcon} from "@/components/icons/ClockIcon.tsx";
import {MusicIcon} from "@/components/icons/MusicIcon.tsx";
import {SquarePlus2Icon} from "@/components/icons";

const PreviewMenu = () => {

  return <div>
    <Menu
      mode="inline"
      defaultSelectedKeys={['recent']}
      onClick={(e) => {
        console.log(e)
        // navigate(e.key)
      }}
      items={[{
        label: "Library",
        type: "group",
        key: "lib",
        children:
          [{label: "Recent", key: "recent", icon: <div className={"mr-3"}><ClockIcon/></div>},
            {label: "Playlist", key: "playlist", icon: <div className={"mr-3"}><MusicIcon/></div>},
            {label: "Create playlist", key: "create", icon: <div className={"mr-3"}><SquarePlus2Icon/></div>}]
      }]}/>
  </div>
}

export default PreviewMenu