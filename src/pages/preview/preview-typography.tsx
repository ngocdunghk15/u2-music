import {Typography} from "antd";

export default function PreviewTypography() {
  return (
    <div>
      <Typography.Title level={1} style={{margin: 0}}>Title level 1 (32px)</Typography.Title>
      <Typography.Title level={2}  style={{margin: 0}}>Title level 2 (28px)</Typography.Title>
      <Typography.Title level={3}  style={{margin: 0}}>Title level 3 (24px)</Typography.Title>
      <Typography.Title level={4}  style={{margin: 0}}>Title level 4 (20px)</Typography.Title>
      <Typography.Title level={5}  style={{margin: 0}}>Title level 5 (18px)</Typography.Title>


      <Typography.Text type="success"> Text (16px)</Typography.Text>
      <Typography.Paragraph type="danger" style={{margin: 0}}>Paragraph (16px)</Typography.Paragraph>
    </div>
  )
}
