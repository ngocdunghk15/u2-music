import {Suspense} from "react";
import {Route, Routes} from "react-router-dom";
import routesConfig from "@/router/routes.tsx";

const Routing = () => {
  return <Suspense>
    <Routes>
      {routesConfig.map((route) => {
        const {wrapper, nestedRoutes} = route;
        const Wrapper = wrapper.element;
        return (
          <Route path={wrapper.path} element={<Wrapper/>}>
            {nestedRoutes.map((routeProps) => (
              <Route {...routeProps} key={routeProps.path as string} />
            ))}
          </Route>
        )
      })}
    </Routes>
  </Suspense>
}

export default Routing;