import PreviewButton from "@/pages/preview/preview-button.tsx";
import PreviewLayout from "@/components/layout-components/PreviewLayout.tsx";
import PreviewMenu from "@/pages/preview/preview-menu.tsx";
import PreviewTypography from "@/pages/preview/preview-typography.tsx";
import PreviewInput from "@/pages/preview/preview-input.tsx";

const routesConfig = [
  {
    wrapper: {
      path: '/preview',
      element: () => <PreviewLayout/>
    },
    nestedRoutes: [
      {
        path: 'button',
        element: <PreviewButton/>
      },
      {
        path: 'typography',
        element: <PreviewTypography/>
      },
      {
        path: 'input',
        element: <PreviewInput/>
      },
       {
        path: 'menu',
        element: <PreviewMenu/>
      }
    ]
  }
]

export default routesConfig
;