import {BrowserRouter as Router} from "react-router-dom";
import './App.css'
import {ConfigProvider, theme} from "antd";
import {ButtonConfig, InputConfig, ModalConfig, SelectConfig, TypographyConfig,tabsConfig} from "./components/styled-components";
import Routing from "@/router/Routing.tsx";

function App() {
  return (
    <ConfigProvider
      theme={{
        components: {
          Form: {},
          Input: InputConfig,
          Button: ButtonConfig,
          Tabs: tabsConfig,
          Menu: {
            itemSelectedBg: 'rgba(255,205,0,0.07)',
            itemSelectedColor:"#FFCD00",
            fontSize:16,
          },
          Typography: TypographyConfig,
          Modal: ModalConfig,
          Select: SelectConfig
        },
        algorithm: theme.defaultAlgorithm,
        token: {
          // Seed Token
          colorPrimary: '#FFCD00',
          fontSize: 16,
          // Alias Token
        },
      }}
    >
      <Router>
        <Routing/>
      </Router>
    </ConfigProvider>
  )
}

export default App
